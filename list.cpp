///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.cpp
/// @version 1.0
///
/// Implementation file for linked list functions
///
/// @author Jonah Bobilin <jbobilin@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   13 April 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <cassert>
#include "list.hpp"

using namespace std;


// return true if list is empty, false if anything in list
const bool DoubleLinkedList::empty() const { 
   return (head == nullptr);
}


// add newNode to front of list
void DoubleLinkedList::push_front( Node* newNode ){
   if (newNode == nullptr)
      return;

   if ( head != nullptr) {
      newNode -> next = head;
      head -> prev = newNode;
      newNode -> prev = nullptr;
      head = newNode;
   } else {
      newNode -> next = nullptr;
      newNode -> prev = nullptr;
      head = newNode;
      tail = newNode;
   }
   count++;
   validate();
}


// add newNode to back of list
void DoubleLinkedList::push_back( Node* newNode){
   if (newNode == nullptr)
      return;

   if ( head != nullptr) {
      tail -> next = newNode;
      newNode->prev = tail;
      newNode->next = nullptr;
      tail = newNode;
   } else {
      newNode -> next = nullptr;
      newNode -> prev = nullptr;
      head = newNode;
      tail = newNode;
   }
   count++;
   validate();
}


//remove a node from the front of the list, if list is already empty return nullptr
Node* DoubleLinkedList::pop_front() { 
   if( head == nullptr)
      return nullptr; 
   
   Node *temp = head;
   if (tail == head){
      head == nullptr;
      tail == nullptr;
      count=1;
      return temp;
   }
   head = head -> next;
   head -> prev = nullptr;
   temp->next = nullptr;

   count--;
   validate();
   return temp;
}


//remove a node from the back of the list, if list is already empty return nullptr
Node* DoubleLinkedList::pop_back() {
   if (tail == nullptr){
      return nullptr;
   }
   
   Node *temp = tail;
   if (tail == head){
      head == nullptr;
      tail == nullptr;
      count=1;
      return temp;
   }

   tail = tail -> prev;
   tail -> next = nullptr;
   temp ->prev = nullptr;

   count--;
   validate();
   return temp;
}


//return the very first node from the list
Node* DoubleLinkedList::get_first() const {
   return head;
}


//return the very last node from the list
Node* DoubleLinkedList::get_last() const {
   return tail;
}


//return the node immediately following currentNode
Node* DoubleLinkedList::get_next( const Node* currentNode ) const {
   if (head == nullptr)
      return nullptr;
   return currentNode -> next;
}


//return the node immediately before currentNode
Node* DoubleLinkedList::get_prev( const Node* currentNode ) const {
   if (head == nullptr)
      return nullptr;
   return currentNode -> prev;
}


//return number of nodes in the list
unsigned int DoubleLinkedList::size() const{
   return count;
}


//returns if node is in list
const bool DoubleLinkedList::isIn(Node* aNode) const {
   Node* currentNode = head;

   while( currentNode != nullptr) {
      if( aNode == currentNode )
         return true;
      currentNode = currentNode->next;
   }

   return false;
}


//insert after
void DoubleLinkedList::insert_after(Node* currentNode, Node* newNode ) {
   if(currentNode==nullptr && head == nullptr) {
      push_front( newNode);
      return;
   }
   
   if(currentNode != nullptr && head == nullptr)
      assert(false );

   if(currentNode == nullptr && head != nullptr)
      assert(false );

   assert (currentNode!=nullptr && head!=nullptr);
   
   assert ( isIn( currentNode ));
   
   assert ( !isIn( newNode ));

   if(tail != currentNode){
      newNode->next = currentNode->next;
      currentNode->next=newNode;
      newNode->prev = currentNode;
      currentNode->next->prev= newNode;
      count++;
   }else{
      push_back(newNode);
   }
   
}


//insert before
void DoubleLinkedList::insert_before( Node* currentNode, Node* newNode) {
     if (head==nullptr){
      head=newNode;
      tail=newNode;
      return;
   }

   if(currentNode == nullptr){
      push_back(newNode);
      return;
   }

   if(head != currentNode){
      newNode->prev = currentNode->prev;
      currentNode->prev=newNode;
      newNode->next = currentNode;
      newNode->prev->next= newNode;
      count++;
   } else {
      push_front(newNode);
   }
}


//swap any two nodes
void DoubleLinkedList::swap(Node* node1, Node* node2){

   //check if empty or only one item
   assert(node1 != nullptr && node2 != nullptr);
   if(size() == 0 || size() == 1)
      return;

   //check if in list and different
   assert(isIn(node1));
   assert(isIn(node2));
   if(node1==node2)
      return;


   //case: only two items in list to swap
   if(size() == 2){
      if(node1 == head){
         head = node2;
         tail = node1;
      } else {
         head = node1;
         tail = node2;
      }
      head->next=tail;
      head->prev=nullptr;
      tail->prev=head;
      tail->next=nullptr;
   }



   //case: node1 is head
   if (node1==head){
      node1->prev = node2->prev;
      node2->prev->next = node1;
      // if node2 is also tail
      if (node2->next != nullptr){
         node2->next->prev = node1;
         node1->next->prev = node2;
         //define temp
         Node* temp = node1->next;
         node1->next = node2->next;
         node2->next = temp;
      } else {
         node2->next = node1->next;
         node1->next->prev = node2;
         node1->next = nullptr;
         tail = node1;
      }
      node2->prev = nullptr;
      head = node2;
      return;
   }

   //case: node1 is tail
   if (node1 == tail){
      node1->next= node2->next;
      node2->next->prev = node1;
       // if node2 is also head
      if (node2->prev != nullptr){
         node2->prev->next = node1;
         node1->prev->next = node2;
         //def temp
         Node* temp = node1 -> prev;
         node1 -> prev = node2->prev;
         node2 -> prev = temp;
      } else {
         node2->prev = node1->prev;
         node1->prev->next = node2;
         node1->prev = nullptr;
         head = node1;
      }
      node2->next = nullptr;
      tail = node2;
      return;
   }


   //case: node2 is head
   if (node2==head){
      node2->prev = node1->prev;
      node1->prev->next = node2;
      //if node1 is also tail
      if (node1->next != nullptr){
         node1->next->prev = node2;
         node2->next->prev = node1;
         //define temp
         Node* temp = node2->next;
         node2->next = node1->next;
         node1->next = temp;
      } else {
         node1->next = node2->next;
         node2->next->prev = node1;
         node2->next = nullptr;
         tail = node2;
      }
      node1->prev = nullptr;
      head = node1;
      return;
   }

   //case: node2 is tail
   if (node2 == tail){
      node2->next= node1->next;
      node1->next->prev = node2;
      //if node1 is also head
      if (node1->prev != nullptr){
         node1->prev->next = node2;
         node2->prev->next = node1;
         //def temp
         Node* temp = node2 -> prev;
         node2 -> prev = node1->prev;
         node1 -> prev = temp;
      } else {
         node1->prev = node2->prev;
         node2->prev->next = node1;
         node2->prev = nullptr;
         head = node2;
      }
      node1->next = nullptr;
      tail = node1;
      return;
   }

   //if neither node are head or tail or both
   //and more than two items in list
   //if nodes are directly next to one another
   if(node1->next == node2 && node2->prev==node1){
      node2->prev=node1->prev;
      node1->prev->next=node2;
      node1->prev=node2;
      node1->next=node2->next;
      node2->next->prev=node1;
      node2->next=node1;
   } else {
      // no special cases
      Node* temp1 = node1->next;
      Node* temp2 = node1->prev;
      node1->next = node2->next;
      node2->next->prev = node1;
      node1->prev = node2 -> prev;
      node2->prev->next = node1;
      node2->next = temp1;
      temp1->prev = node2;
      node2->prev= temp2;
      temp2->next=node2;
   }
}


//test if list is sorted (copied from slides)
const bool DoubleLinkedList::isSorted() const{
   if (count <= 1 )
      return true;
   
   for( Node* i = head ; i->next != nullptr ; i = i -> next ){
      if(*i > *i -> next)
         return false;
   }

   return true;
}


//insertion sort, sorts list
void DoubleLinkedList::insertionSort(){

   //if already sorted, or empty, or list is one item do nothing
   if(isSorted() || head == nullptr || head == tail)
      return;

   //declare variables (for any two nodes current is followed by index)
   Node* current = nullptr;
   Node* index = nullptr;

   //loop through list until sorted
   for( current=head; current->next != nullptr; current = current->next){
      //declare temporary container to hold the lower number
      Node* temp = current;
      //loop and update if index is less than next number
      for( index = current->next; index != nullptr; index = index->next){
         if( *temp > *index)
            temp = index;
      }
      //swap the lower with current and reset current marker
      swap(current, temp);
      current = temp;
   }
}

// validation program utility (copied from slides) 
bool DoubleLinkedList::validate() const{
   if(head == nullptr){
      assert( tail == nullptr);
      assert( count == 0);
   } else {
      assert( tail != nullptr);
      assert( count != 0);
   }

   if(tail == nullptr){
      assert( head == nullptr);
      assert( count == 0);
   } else {
      assert( head != nullptr);
      assert( count != 0);
   }
   
   if( head != nullptr && tail == head){
      assert( count == 1);
   }

   unsigned int forwardCount = 0;
   Node* currentNode = head;
   //count forward through list
   while(currentNode != nullptr) {
      forwardCount++;
      if( currentNode -> next != nullptr){
         assert( currentNode->next->prev == currentNode);
      }
      currentNode = currentNode -> next;
   }
   assert( count == forwardCount);

   //count backwards through list
   unsigned int backwardCount = 0;
   currentNode = tail;
   while(currentNode != nullptr) {
      backwardCount++;
      if( currentNode->prev != nullptr) {
         assert( currentNode->prev->next == currentNode );
      }
      currentNode = currentNode->prev;
   }
   assert(count == backwardCount);

//   cout << "List is valid" << endl;

   return true;
}

