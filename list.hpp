///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.hpp
/// @version 1.0
///
/// Exports data about all nene birds
///
/// @author Jonah Bobilin <jbobilin@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   12 April 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <iostream>
#include "node.hpp"

using namespace std;

class DoubleLinkedList {

protected:
   Node* head = nullptr;
   Node* tail = nullptr;
   unsigned int count = 0;

public:
   const bool empty() const;
   void push_front( Node* newNode );
   void push_back( Node* newNode );
   Node* pop_front();
   Node* pop_back();
   Node* get_first() const;
   Node* get_last() const;
   Node* get_next( const Node* currentNode ) const;
   Node* get_prev( const Node* currentNode ) const;
   unsigned int size() const;
  
   const bool isIn(Node* aNode) const;
   void insert_after(Node* currentNode, Node* newNode);
   void insert_before(Node* currentNode, Node* newNode);

   //swaps and sorts
   void swap(Node* node1, Node* node2);
   const bool isSorted() const;
   void insertionSort();

   //validation
   bool validate() const;

};

