###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 08a - Cat Wrangler
#
# @file    Makefile
# @version 1.0
#
# @author Jonah Bobilin <jbobilin@hawaii.edu>
# @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
# @date   12 April 2021
###############################################################################

CXX      = g++
CXXFLAGS = -std=c++20    \
           -O3           \
           -Wall         \
           -pedantic     \
           -Wshadow      \
           -Wconversion

all: main

main.o:  main.cpp cat.hpp
	$(CXX) -c $(CXXFLAGS) $<

cat.o: cat.cpp cat.hpp
	$(CXX) -c $(CXXFLAGS) $<

list.o: list.hpp list.cpp
	$(CXX) -c list.cpp

node.o: node.cpp node.hpp
	$(CXX) -c $(CXXFLAGS) $<

main: *.hpp main.o cat.o list.o node.o
	$(CXX) -o $@ main.o cat.o list.o node.o

queueSim.o: queueSim.cpp *.hpp
	$(CXX) -c $(CXXFLAGS) $<

queueSim: *.hpp queueSim.o list.o node.o
	$(CXX) -o $@ queueSim.o list.o node.o

test.o: test.cpp
	$(CXX) -c $(CXXFLAGS) $<

test: *.hpp test.o list.o cat.o node.o
	$(CXX) -o $@ test.o list.o cat.o node.o

clean:
	rm -f *.o main queueSim test
